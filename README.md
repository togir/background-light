# Background Light

This small programm changes the intensity of the display background-light.
It only works for intel graphic drivers that exposes

    /sys/class/backlight/intel_backlight/brightness
    /sys/class/backlight/intel_backlight/max_brightness

Also the file needs to be writeable by the current user.
Notice that the files in /sys are recreated on every reboot so you need some task to change the filepermissions after reboot.
This can work as example to start with:

    # /etc/crontab: system-wide crontab

    # Example of job definition:
    # .---------------- minute (0 - 59)
    # |  .------------- hour (0 - 23)
    # |  |  .---------- day of month (1 - 31)
    # |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...
    # |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
    # |  |  |  |  |
    # *  *  *  *  * user-name command to be executed
    @reboot		root	chmod 666 /sys/class/backlight/intel_backlight/brightness

## Installation
clone the repo and copy the file bgl to /usr/local/bin or your local path ~./local/bin

## Usage

    // Display the current brigthness
    bgl

    // Increase brightness by 10%
    bgl +10 

    // Decrease brightness by -10%
    bgl -10

    // Set the brightness to 40%
    bgl 40

    // Show the help
    bgl -h
    bgl --help
